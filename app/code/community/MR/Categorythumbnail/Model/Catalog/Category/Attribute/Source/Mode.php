<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 29/01/15
 * Time: 11:31 AM
 */
class MR_Categorythumbnail_Model_Catalog_Category_Attribute_Source_Mode extends Mage_Catalog_Model_Category_Attribute_Source_Mode {

    const DM_THUMBNAIL_ONLY     = 'THUMBNAIL_ONLY';
    const DM_THUMBNAIL_PRODUCT  = 'THUMBNAIL_AND_PRODUCT';
    const DM_THUMBNAIL_BESTSELLERS = 'THUMBNAIL_AND_BESTSELLERS';


    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = array(
                array(
                    'value' => Mage_Catalog_Model_Category::DM_PRODUCT,
                    'label' => Mage::helper('catalog')->__('Products only'),
                ),
                array(
                    'value' => Mage_Catalog_Model_Category::DM_PAGE,
                    'label' => Mage::helper('catalog')->__('Static block only'),
                ),
                array(
                    'value' => Mage_Catalog_Model_Category::DM_MIXED,
                    'label' => Mage::helper('catalog')->__('Static block and products'),
                ),
                array(
                    'value' => self::DM_THUMBNAIL_ONLY,
                    'label' => Mage::helper('catalog')->__('Sub category thumbnails only'),
                ),
                array(
                    'value' => self::DM_THUMBNAIL_PRODUCT,
                    'label' => Mage::helper('catalog')->__('Sub category thumbnails and products'),
                ),
                array(
                    'value' => self::DM_THUMBNAIL_BESTSELLERS,
                    'label' => Mage::helper('catalog')->__('Sub category thumbnails and Bestsellers'),
                ),
            );
        }
        return $this->_options;
    }
}

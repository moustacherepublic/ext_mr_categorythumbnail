<?php

class MR_Categorythumbnail_Block_Thumbnails extends Mage_Core_Block_Template{
        public function getSubCategories(){
            //$childrenCategories = Mage::helper('mr_categorythumbnail')->getCurrentCategory()->getChildrenCategories()->addAttributeToSelect('thumbnail');
                $childrenCategories = Mage::helper('mr_categorythumbnail')
                    ->getCurrentCategory()
                    ->getChildrenCategoriesWithInactive()
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToSelect('thumbnail');

                return $childrenCategories;
        }
}

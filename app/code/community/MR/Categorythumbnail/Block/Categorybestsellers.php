<?php

class MR_Categorythumbnail_Block_Categorybestsellers extends Mage_Catalog_Block_Product_New{

    public function getCacheKeyInfo()
    {
        return array(
            'MR_HOME_PRODUCT_FEATURED',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            $this->getProductsCount(),
            Mage::registry('current_category')->getId(),
        );
    }

    protected function _getProductCollection()
    {
        $currentCategory = Mage::registry('current_category');

        if($currentCategory && $currentCategory->getId()){
            $now = Mage::app()->getLocale()->date();
            $to= Mage::app()->getLocale()->utcDate(NULL,$now); //now
            $from = Mage::app()->getLocale()->utcDate(NULL,$now)->subDay(365);

            /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
            $collection = Mage::getResourceModel('mr_categorythumbnail/reports_product_collection');
            $collection = $this->_addProductAttributesAndPrices($collection)
                ->addStoreFilter()
                ->addOrderedQty($from->toString(Varien_Date::DATETIME_INTERNAL_FORMAT),$to->toString(Varien_Date::DATETIME_INTERNAL_FORMAT))
                ->addAttributeToFilter('status',  array(Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                ->addCategoryFilter($currentCategory)
                ->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
                ->setOrder('ordered_qty', 'desc')
                ->setPageSize($this->getProductsCount())
                ->setCurPage(1);
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

            return $collection;
        }else{
            return false;
        }
    }

    public function getCacheLifetime()
    {
        return 0;
    }
}

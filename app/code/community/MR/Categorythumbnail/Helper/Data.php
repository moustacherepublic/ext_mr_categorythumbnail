<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 29/01/15
 * Time: 11:29 AM
 */
class MR_Categorythumbnail_Helper_Data extends Mage_Core_Helper_Abstract {

        public function getCurrentCategory()
        {
                return Mage::registry('current_category');

        }

        public function isThumbnailOnlyMode(){

                $category = $this->getCurrentCategory();
                $res = false;
                if ($category->getDisplayMode() == MR_Categorythumbnail_Model_Catalog_Category_Attribute_Source_Mode::DM_THUMBNAIL_ONLY) {
                        $res = true;
                        if ($category->getIsAnchor()) {
                                $state = Mage::getSingleton('catalog/layer')->getState();
                                if ($state && $state->getFilters()) {
                                        $res = false;
                                }
                        }
                }
                return $res;
        }

        public function isThumbnailProductMode(){
                return $this->getCurrentCategory()->getDisplayMode() == MR_Categorythumbnail_Model_Catalog_Category_Attribute_Source_Mode::DM_THUMBNAIL_PRODUCT;

        }

        public function isThumbnailBestsellersMode(){
                return $this->getCurrentCategory()->getDisplayMode() == MR_Categorythumbnail_Model_Catalog_Category_Attribute_Source_Mode::DM_THUMBNAIL_BESTSELLERS;
        }
}
